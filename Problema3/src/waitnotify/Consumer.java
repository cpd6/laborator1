package waitnotify;

import queue.Data;

public class Consumer extends Thread{

    private long delay;
    private Data data;

    public Consumer(long delay, Data data) {
        super();
        this.delay = delay;
        this.data = data;
    }

    @Override
    public void run() {
        try {
            String docs = null;
            while(!"end".equals(docs)) {
                System.out.println("Consumer asking for doc");
                String doc = data.consume();
                sleep(delay);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}