package waitnotify;

import queue.Consumer;
import queue.Data;
import queue.Producer;

import java.util.ArrayList;
import java.util.Random;

public class Printer {
    public static void execute() {
        Data docs = new Data();
        Random r = new Random();
        ArrayList<queue.Producer> producers = new ArrayList();

        System.out.println("Initiaalize threads");

        queue.Consumer c = new Consumer(2000L, docs);

        for (int i = 0; i < 7; i++) {
            producers.add(new queue.Producer(r.nextInt(1000) + 3000,r.nextInt(1) + 10, i, docs));
        }

        System.out.println("Start threads synchronized by queue");

        for (Producer prod : producers) {
            prod.start();
        }
        c.start();
    }
}
