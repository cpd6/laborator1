package waitnotify;

public class Data {
    String doc;
    boolean producing = false;

    public synchronized void produce(String doc) {
        while(!producing) {
            try {
                System.out.println("Docs produce is waiting");
                wait();
                System.out.println("Docs produce was notified");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Data replacing" + this.doc + " with " + doc);
        this.doc = doc;
        producing = false;
        System.out.println("Data producer notifies all threads");
        notifyAll();
    }

    public synchronized String consume() {
        while (producing) {
            try {
                System.out.println("Docs consumer is waiting");
                wait();
                System.out.println("Docs consumer was notified");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Docs consuming " + this.doc);
        System.out.println("Docs consumer notifies all threads");
        notifyAll();
        producing = true;
        return doc;
    }
}
