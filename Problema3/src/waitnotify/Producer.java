package waitnotify;

import queue.Data;

public class Producer extends Thread {

    private long delay;
    private int pages;
    private Data data;
    private int clerkNumber;

    public Producer(long delay, int pages, int clerkNumber, Data data) {
        super();
        this.delay = delay;
        this.data = data;
        this.pages = pages;
        this.clerkNumber = clerkNumber;
    }

    @Override
    public void run() {
        try {
            for (int i=0; i<= pages; i++) {
                sleep(delay);
                String doc;
                if (i == pages) {
                    doc="end";
                } else {
                    doc = "doc"+i + " (clerk" + clerkNumber+" time: "+delay+")";
                }
                System.out.println("Producing: " + doc);
                data.produce(doc);
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}