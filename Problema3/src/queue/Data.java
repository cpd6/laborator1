package queue;

import java.util.LinkedList;
import java.util.Queue;

public class Data {

    Queue<String> queue = new LinkedList<>();

    public void produce(String doc) {
        System.out.println("Add document..");
        queue.add(doc);
    }

    public String consume() {
        String doc = queue.poll();
        System.out.println("Printing document..");
        return doc;
    }

    public boolean hasDocs() { return !queue.isEmpty(); }


}
