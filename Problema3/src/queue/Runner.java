package queue;

import queue.Consumer;
import queue.Data;
import queue.Producer;

import java.util.ArrayList;
import java.util.Random;

public class Runner {

    public static void execute() {
        Data queue = new Data();
        Random r = new Random();
        ArrayList<Producer> producers = new ArrayList();

        System.out.println("Initiaalize threads");

        Consumer c = new Consumer(2000L, queue);

        for (int i = 0; i < 7; i++) {
            producers.add(new Producer(r.nextInt(1000) + 3000,r.nextInt(1) + 10, i, queue));
        }

        System.out.println("Start threads synchronized by queue");

        for (Producer prod : producers) {
            prod.start();
        }
        c.start();
    }
}
