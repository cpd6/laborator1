package queue;

public class Consumer extends Thread{

    private long delay;
    private Data data;

    public Consumer(long delay, Data data) {
        super();
        this.delay = delay;
        this.data = data;
    }

    @Override
    public void run() {
        try {

            while(!data.hasDocs()) {
                System.out.println("Nothing to print");
                sleep(500);
            }

            while(data.hasDocs()) {
                String doc = data.consume();
                System.out.println("Consumer reads: " + doc);
                sleep(delay);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}