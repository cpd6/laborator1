import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Data {
  boolean writing = false;
  private String fileName;

  public Data(String fileName) {
     this.fileName = fileName;
  }

  public synchronized void produce(String packet) {
    while(!writing) {
      try {
        System.out.println("Data produce is waiting");
        wait();
        System.out.println("Data produce was notified");
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    try {
      FileWriter myWriter = new FileWriter(fileName);
      myWriter.write(packet);
      myWriter.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    writing = false;
    System.out.println("Data producer notifies all threads");
    notifyAll();
  }

  public synchronized String consume() {
    while (writing) {
      try {
        System.out.println("Data consumer is waiting");
        wait();
        System.out.println("Data consumer was notified");
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    String packet="";
    try (FileReader reader = new FileReader("writeread.txt")) {
      int character;

      while ((character = reader.read()) != -1) {
        packet = packet + (char) character;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println("Data consuming packet " + packet);
    System.out.println("Data consumer notifies all threads");
    notifyAll();
    writing = true;
    return packet;
  }
}
