import java.io.File;
import java.io.IOException;

public class LockSyncRunner {

  public static void run() {
    Data dataObj = new Data("writeread.txt");

    File file = null;
    try {
      file = new File("writeread.txt");
      if (file.createNewFile()) {
        System.out.println("File created: " + file.getName());
      } else {
        System.out.println("File already exists.");
      }
    } catch (IOException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }

    System.out.println("Initialize threads synchronized by lock");
    Producer p = new Producer(1000, dataObj);
    Consumer c = new Consumer(3000, dataObj);

    System.out.println("Start threads synchronized by lock");
    p.start();
    c.start();
  }
}
